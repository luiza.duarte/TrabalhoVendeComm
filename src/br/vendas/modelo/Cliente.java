package br.vendas.modelo;

import java.util.ArrayList;
import java.util.List;

public class Cliente {
	private int cpf;
	private String nome;
	private String telefone;
	private String dataCadastro;
	private Endereco tipo;
	
	private List<Compra> compras = new ArrayList<Compra>();
	
	public void adicionarCompra(Compra compra){
		compras.add(compra);
	}
		
	
	public int getCpf() {
		return cpf;
	}

	public void setCpf(int cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(String dataCadastro) {
		this.dataCadastro = dataCadastro;
	}


	public Cliente(int cpf, String nome, String telefone, String dataCadastro,Endereco tipo) {
		this.cpf = cpf;
		this.nome = nome;
		this.telefone = telefone;
		this.dataCadastro = dataCadastro;		
		this.tipo = tipo;
	}
	
	public List<Compra> getCompras(){
		return compras;
	}
	
	public void removerCompraPorNumero(int numero) {
		for(Compra compra:compras) {
			if(compra.getNumero() == numero) {
				compras.remove(numero);
			}
		}
	}
	
	public Compra pegarCompraPorNumero(int numero) {
		for(Compra compra: compras) {
			if(compra.getNumero() == numero) {
				return compra;
			}
		}
		return null;
	}


	public Endereco getTipo() {
		return tipo;
	}


	public void setTipo(Endereco tipo) {
		this.tipo = tipo;
	}
	
	

}
