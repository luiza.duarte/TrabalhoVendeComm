package br.vendas.modelo;

public class ItemCompra {
	
	
	private int quantidade;
	
	private Produto produto = new Produto(quantidade, null, quantidade, quantidade);
	
	public ItemCompra(String descricaoProduto,int codigo, double preco, int quantidade){
		produto.setCodigo(codigo);
		produto.setDescricaoProduto(descricaoProduto);
		this.quantidade = quantidade;
		produto.setPreco(preco);
	}
	
	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
}

	
