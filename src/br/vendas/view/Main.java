package br.vendas.view;
import br.vendas.control.Control;
import br.vendas.modelo.Endereco;

public class Main {
	
	public static void main(String[] args) {
			
		Control controlador = Control.getInstance();
		
		controlador.cadastrarProduto(1, "Mouse", 10, 20);
		controlador.cadastrarProduto(2, "teclado", 10, 30);
		controlador.cadastrarProduto(3, "monitor", 50, 200);
		
		controlador.cadastrarCliente(16515651, "Maicon", "995262", "30/04/2018",Endereco.RESIDENCIAL);
		controlador.cadastrarCliente(61651561, "Luiza", "952262", "30/04/2018",Endereco.COMERCIAL);
		controlador.cadastrarCliente(5156161, "Francisco", "651561", "30/04/2018", Endereco.RESIDENCIAL);
		
		for(String srt: controlador.selecionarProdutosCadastrado()) {
			System.out.println(srt);
		}
		System.out.println("\n\n");		
		for (String str: controlador.selecionarClientesCadastrado()){
			System.out.println(str);
		}

		controlador.selecionarClienteNome("Maicon");
		controlador.abrirCompra(2);
		controlador.venderProduto(1, 1, "Mouse", 2);
		controlador.venderProduto(2, 2, "teclado", 2);
		controlador.venderProduto(3, 3, "monitor", 2);
		System.out.println("Total: "+controlador.totalizarVenda(1));
	}
	
}
